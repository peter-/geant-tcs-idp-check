#!/usr/bin/env python3
"""
Check the GÉANT TCS SAML Service Provider for missing Identity Providers.
-- peter@aco.net
"""
import requests
import re
import argparse
import json
import urllib3
from bs4 import BeautifulSoup
from time import sleep
from lxml import etree

get_url = 'https://www.digicert.com/secure/saml/discovery/?entityID=https%3A%2F%2Fwww.digicert.com%2Fsso&returnIDParam=idp'
post_url = 'https://www.digicert.com/sso/saml/login?disco=true'


def get_csrf_token(get_url=get_url):
    r = requests.get(get_url, timeout=5)
    soup = BeautifulSoup(r.content, "lxml")
    f = soup.find("form", id="go")
    inputs = f.findChildren("input", type="hidden")
    for x in inputs:
        try:
            if x.attrs['name'] == 'csrf_token':
                token = x.attrs['value']
                break
        except KeyError:
            pass
    return token


def ask_about_idp(idp, token, use_csrf_token=False, post_url=post_url):
    headers = {
        'Origin': 'https://www.digicert.com',
        'Upgrade-Insecure-Requests': '1',
        'Content-Type': 'application/x-www-form-urlencoded',
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0',
        'Accept': 'text/html,application/xhtml+xml,application/xml',
    }
    data = {
        'idp': idp,
        'post_location': '/secure/saml/discovery/?entityID=https%3A%2F%2Fwww.digicert.com%2Fsso&returnIDParam=idp',
        'post_url': '/secure/saml/discovery/',
    }
    if use_csrf_token:
        data['csrf_token'] = token
    r = requests.post(post_url, timeout=5, data=data, headers=headers, allow_redirects=False)
    return r


def get_idps(md, insecure, regauth=None):
    if insecure:
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    verify = not insecure
    r = requests.get(md, timeout=30, verify=verify).content
    tree = etree.fromstring(r)
    idps = []
    NS = {"md": "urn:oasis:names:tc:SAML:2.0:metadata", "mdrpi": "urn:oasis:names:tc:SAML:metadata:rpi"}
    xpath = '//md:EntityDescriptor[md:IDPSSODescriptor]'
    if regauth:
        xpath = xpath[:-1] + ' and md:Extensions/mdrpi:RegistrationInfo/@registrationAuthority="{}"]'.format(regauth)
    for e in tree.xpath(xpath, namespaces=NS):
        idps.append(e.get('entityID'))
    idps.sort()
    return idps


def check_idps(idps, use_csrf_token, quiet, pause):
    unknown = []
    for idp in idps:
        if use_csrf_token:
            token = get_csrf_token()
        else:
            token = None
        r = ask_about_idp(idp, token, use_csrf_token)

        if r.status_code == 500:
            soup = BeautifulSoup(r.content, "lxml")
            text = soup.find("pre").text.strip()
            m = re.match("javax\.servlet\.ServletException.*Metadata for entity {} and role.*$".format(idp), text)
            if m:
                if not quiet:
                    print(idp)
                unknown.append(idp)
        elif r.status_code == 302:
            pass
        else:
            # Shouldn't happen
            print(r.status_code)
            print(r.content)
        if idp != idps[-1]:
            sleep(pause)
    return unknown


def get_args():
    parser = argparse.ArgumentParser(description='Test the GÉANT TCS SAML Service Provider for missing Identity Providers')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-m', type=str, dest='md', metavar='MD-URL', help='URL to SAML 2.0 Metadata to get IDP entityIDs from')
    group.add_argument('-i', type=str, dest='idp', action='append', metavar='IDP-ENTITYID', help='entityID of an IDP to look for, use multiple times for multiple IDPs')
    group.add_argument('-f', type=str, dest='file', help='File to get IDP entityIDs from, one per line')
    parser.add_argument('-r', type=str, dest='regauth', help='Optionally filter metadata for this registrationAuthority')
    parser.add_argument('-s', type=float, default=0.5, metavar='SLEEP', dest='pause', help='Seconds (int or float) to sleep between each request')
    parser.add_argument('-c', '--csrf', action='store_true', dest='use_csrf_token', help='Use unique CSRF token on each request. SLOW!')
    parser.add_argument('-j', '--json', action='store_true', help='Only output missing IDPs in JSON format, be quiet otherwise')
    parser.add_argument('--insecure', action='store_true', help='Ignore TLS security when downloading metadata with -m')
    args = parser.parse_args()
    return (parser, args)


def main():
    p, a = get_args()
    if a.idp:
        idps = a.idp
        if not a.json:
            print("Checking for missing IDPs using {} specified entityID{}:".format(len(idps), '' if len(idps) == 1 else 's'))
    elif a.file:
        f = open(a.file, 'r')
        s = f.read()
        f.close
        idps = s.strip().splitlines()
        if not a.json:
            print("Checking for missing IDPs using {} found in file {}:".format(len(idps), a.file))
    elif a.md:
        idps = get_idps(a.md, a.insecure, a.regauth)
        if not a.json:
            print("Checking for missing IDPs using {} found in metadata {}:".format(len(idps), a.md))
    else:
        p.print_usage()
        p.exit()

    unknown = check_idps(idps, a.use_csrf_token, a.json, a.pause)
    if unknown:
        if a.json:
            print(json.dumps(unknown))
        else:
            print("Result: {} out of {} IDP{} missing".format(len(unknown), len(idps), '' if len(idps) == 1 else 's'))
            # for idp in unknown:
            #    print(idp)
        p.exit(1)
    else:
        if not a.json:
            print("Result: None missing :)")


if __name__ == "__main__":
    main()
