GÉANT TCS Identity Provider check
=================================

Check the current provider of [GÉANT TCS](https://www.geant.org/Services/Trust_identity_and_security/Pages/TCS.aspx) for missing SAML Identity Providers.

For debugging and diagnostic purposes only.

Usage
-----

```sh
usage: idp-check.py [-h] [-m MD-URL | -i IDP-ENTITYID | -f FILE] [-r REGAUTH]
                    [-s SLEEP] [-c] [-j] [--insecure]

Test the GÉANT TCS SAML Service Provider for missing Identity Providers

optional arguments:
  -h, --help       show this help message and exit
  -m MD-URL        URL to SAML 2.0 Metadata to get IDP entityIDs from
  -i IDP-ENTITYID  entityID of an IDP to look for, use multiple times for
                   multiple IDPs
  -f FILE          File to get IDP entityIDs from, one per line
  -r REGAUTH       Optionally filter metadata for this registrationAuthority
  -s SLEEP         Seconds (int or float) to sleep between each request
  -c, --csrf       Use unique CSRF token on each request. SLOW!
  -j, --json       Only output missing IDPs in JSON format, be quiet otherwise
  --insecure       Ignore TLS security when downloading metadata with -m
```

This tool uses a provided list of SAML 2.0 Identity Provider `entityIDs` and tries to initiate WebSSO at the SP with each entityID in turn. IDP entityIDs can be extracted automatically from remote SAML 2.0 Metadata (in which case that metadata can also be filtered for a certain `registrationAuthority`, cf. [SAML MetadataDRI](https://wiki.oasis-open.org/security/SAML2MetadataDRI)), from a local plain text file (containing one entityID per line) or from command line arguments (repeat option `-i` for each IDP to add).

Unless the `--json` option is used some informative/decorative output is added, including the number of IDPs to process and how many of those were found to be unknown.

If at least one IDP was found to be unknown the tool will exit with a non-zero exit code.  
If no IDP triggered an error at the SP the exit code will be zero.

Examples
--------

> N.B.: Testing only makes sense for IDPs that are *expected to be known* to the SP, i.e., IDPs in [eduGAIN](https://edugain.org/) and institutions that are part of [NREN](https://en.wikipedia.org/wiki/NREN)s that [participate in GÉANT TCS](https://wiki.geant.org/display/TCSNT/TCS+Participants).  
As all of the examples below are fictitious (they only illustrate usage of the tool) those trying to pull metadata from non-existing URLs will obviously fail. And all entityIDs enumerated below would be reported as unknown. [D'oh!](https://en.wikipedia.org/wiki/Garbage_in,_garbage_out)

Get entityIDs for SAML IDPs from a SAML 2.0 Metadata document (an "aggregate" or a "federation" or a "feed") and test each at the SP:

```sh
./idp-check.py -m https://federation.example.org/metadata.xml
```

Same as above but additionally limit the selection of IDPs to those that have a given `registrationAuthority` value, i.e., that are from a specific "federation":

```sh
./idp-check.py -m https://interfederation.example.org/md.xml -r http://reg.example.com/
```

Test the IDPs with the provided entityIDs only (no metadata URL or lookup required):

```sh
./idp-check.py -i http://example.org/saml -i https://sso.example.com -i urn:mace:unexists
```

Test the IDPs contained in the provided local text file (one per line; no metadata URL or lookup required):

```sh
./idp-check.py -f myfile
```

Modify sleep time (a forced pause after each request to the SP) from its default value (0.5 seconds) to 2 seconds:

```sh
./idp-check.py -s2 -m https://federation.example.org/metadata.xml
```

Aquire and supply a new [CSRF](https://en.wikipedia.org/wiki/CSRF) Token with every single request to the SP, as implemented by the current TCS SP website: (Note that this *significantly* slows down processing due to the slowness of the URL the token needs to be aquired from first)

```sh
./idp-check.py --csrf -m https://federation.example.org/metadata.xml
./idp-check.py -c -i https://sso.example.com
```

Be quiet (suppress informational/decorative output) and only report unknown IDPs in a JSON list after having finished processing:

```sh
./idp-check.py -j -c -i http://example.org/saml -i https://sso.example.com
["http://example.org/saml", "https://sso.example.com"]
```
